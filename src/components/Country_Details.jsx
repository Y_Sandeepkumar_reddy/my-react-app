import React, { useEffect, useState } from "react";
import { useParams } from "react-router-dom";
// import { Link } from "react-router-dom";
import { BrowserRouter as Router,Route,Routes,Link } from "react-router-dom";


function Country_Details() {
  const { name } = useParams();
  const [country, setCountry] = useState(null);
  const [loading, setLoading] = useState(true);

  useEffect(() => {
    const fetchCountryDetails = async () => {
      try {
        const response = await fetch(
          `https://restcountries.com/v3.1/name/${name}`
        );
        if (!response.ok) {
          throw new Error("Network response was not ok");
        }
        const data = await response.json();
        setCountry(data[0]);
      } catch (error) {
        console.error(error.message);
      } finally {
        setLoading(false);
      }
    };

    fetchCountryDetails();
  }, [name]);

  if (loading) return <p>Loading...</p>;
  if (!country) return <p>Country not found</p>;

  const {
    name: { common },
    flags: { png: flag },
    population,
    region,
    capital,
    subregion,
    languages,
    currencies,
  } = country;

  return (
    <>
      <div className="max-width">
        <Link to={"/"}>
          <div className="back-btn">
            <i className="fa-solid fa-arrow-left-long "></i> Back
          </div>
        </Link>

        <div className="country_card">
          <div className="country_img-div">
            <img src={flag} alt={common} className="country_img" />
          </div>
          <div className="country_details">
            <h1 className="country_name">{common}</h1>
            <p>Population: {population.toLocaleString()}</p>
            <p>Region: {region}</p>
            <p>Subregion: {subregion}</p>
            <p>Capital: {capital}</p>
            <p>Languages: {Object.values(languages).join(", ")}</p>
            <p>
              Currencies:{" "}
              {Object.values(currencies)
                .map((c) => c.name)
                .join(", ")}
            </p>
          </div>
        </div>
      </div>
    </>
  );
}

export default Country_Details;
