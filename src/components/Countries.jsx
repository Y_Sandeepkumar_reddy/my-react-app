  import React, { useEffect, useState } from "react";
  import { Link } from "react-router-dom";

  function Countries({ search, region }) {
    const [countries, setCountries] = useState([]);
    const [loading, setLoading] = useState(false);

    useEffect(() => {
      fetchCountryData();
    }, []);

    const fetchCountryData = async () => {
      try {
        setLoading(true);
        const response = await fetch("https://restcountries.com/v3.1/all");
        if (!response.ok) {
          throw new Error("Network response was not ok");
        }
        const data = await response.json();
        setCountries(data);
      } catch (error) {
        console.error(error.message);
      } finally {
        setLoading(false);
      }
    };

    let filterCountries = countries;
    if (search) {
      filterCountries = filterCountries.filter((ele) =>
        ele?.name?.common.toLowerCase().includes(search.toLowerCase())
      );
    }
    if (region) {
      filterCountries = filterCountries.filter((ele) =>
        ele?.region.toLowerCase().includes(region.toLowerCase())
      );
    }

    return (
      <div className="max-width">
        <section className="allCountryDiv">
          {loading ? (
            <p>Loading...</p>
          ) : (
            filterCountries.map((country, index) => {
              const {
                name,
                flags: { png: flag },
                population,
                region,
                capital,
              } = country;

              return (
                <article key={index}>
                  <div className="countryCard">
                    <Link to={`/Countries/${name.common}`}>
                      <img className="image" src={flag} alt={name.common} />
                    </Link>
                    <div className="details">
                      <h3>{name.common}</h3>
                      <h4>
                        Population: <span>{population.toLocaleString()}</span>
                      </h4>
                      <h4>
                        Region: <span>{region}</span>
                      </h4>
                      <h4>
                        Capital: <span>{capital}</span>
                      </h4>
                    </div>
                  </div>
                </article>
              );
            })
          )}
        </section>
      </div>
    );
  }

  export default Countries;
