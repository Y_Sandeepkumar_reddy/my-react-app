import React from "react";

function Filter({ handleSearch, handleRegion }) {

  const handler = (e) => {
    handleSearch(e.target.value.toLowerCase());
  };
  const handleReg = (e) => {
    handleRegion(e.target.value.toLowerCase());
  };

  return (
    <div className="max-width">
      <section className="filter">
        <form className="form-control">
          <input
            type="search"
            name="search"
            id="search"
            placeholder="Search for country"
            onChange={handler}
            className="input"
          />
        </form>

        <div className="region-filter">
          <select name="select" id="select" className="select" onChange={handleReg}>
            <option value="Filter by region">Filter by region</option>
            <option value="Africa">Africa</option>
            <option value="America">America</option>
            <option value="Asia">Asia</option>
            <option value="Europe">Europe</option>
            <option value="Oceania">Oceania</option>
          </select>
        </div>
      </section>
    </div>
  );
}

export default Filter;
