import React, { useState } from "react";

function Header() {

const [darkTheme,setDarkTheme]=useState(false)

  const changeTheme=()=>{
    document.body.classList.toggle('light');
    setDarkTheme(!darkTheme);
  }
  return (
    <div className="max-width">
      <section>
        <header className="header">
          <div>
            <h1>Where in the world ?</h1>
          </div>
          <div onClick={changeTheme} className="light">
            <i className="fa-solid fa-moon"></i> Dark moode
          </div>
        </header>
      </section>
    </div>
  );
}

export default Header;
