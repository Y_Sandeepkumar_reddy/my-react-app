
import {
  BrowserRouter as Router,
  Route,
  Routes,
  useLocation,
} from "react-router-dom";
import { useState } from "react";
import "./App.css";
import Countries from "./components/Countries";
import Filter from "./components/Filter";
import Header from "./components/Header";
import Country_Details from "./components/Country_Details";

function App() {
  const [search, setSearch] = useState("");
  const [region, setRegion] = useState("");

  const handleSearch = (inputValue) => {
    setSearch(inputValue);
  };

  const handleRegion = (inputValue) => {
    setRegion(inputValue);
  };

  return (
    <Router>
      <Header />
      <AppRoutes
        search={search}
        region={region}
        handleSearch={handleSearch}
        handleRegion={handleRegion}
      />
    </Router>
  );
}

function AppRoutes({ search, region, handleSearch, handleRegion }) {
  const location = useLocation();

  // Determine if we are on the Country_Details page
  const isCountryDetailsPage = location.pathname.startsWith("/Countries/");

  return (
    <>
      {!isCountryDetailsPage && (
        <Filter handleSearch={handleSearch} handleRegion={handleRegion} />
      )}
      <Routes>
        <Route
          path="/"
          element={<Countries search={search} region={region} />}
        />
        <Route path="/Countries/:name" element={<Country_Details />} />
      </Routes>
    </>
  );
}

export default App;
